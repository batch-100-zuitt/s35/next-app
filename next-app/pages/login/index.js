import { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../../UserContext';
import usersData from '../../data/usersdata';
import Router from 'next/router';



export default function index() {
	// const [user, setUser] = useState('');
	const { setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isAdmin, setIsAdmin] = useState('false');

	function authenticate(e) {

		//prevent redirection via form submission
		e.preventDefault();

		

		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body:JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)


			if(data.accessToken){
				localStorage.setItem('token',data.accessToken);
				fetch('http://localhost:4000/api/users/details',{
					headers:{
						Authorization: `Bearer ${data.accessToken}`
					}
				})

				.then(res => {
					return res.json();
				})

				.then(data => {
					localStorage.setItem("id", data._id)
            		localStorage.setItem("isAdmin", data.isAdmin)
            		 
            		 let adminUser = localStorage.getItem("isAdmin");

            		 if (adminUser == "false" || !adminUser){
            		 		Router.push('/courses')
            		 }
            		 else{
            		 		Router.push('/#')
            		 }
				})
			} else {
				alert('Wrong email or password');
			}
		})

			    setEmail('');
		    setPassword('');
	}



		// if conditions are met will return true
		// if conditions are not met will return false
		// also returns the object that meets the condition
		/*
		const match = usersData.find(user => {
		    return (user.email === email && user.password === password);
		})

		if(match){
		    localStorage.setItem('email', email);
		    localStorage.setItem('isAdmin', match.isAdmin);

		    setUser({
		        // email: localStorage.getItem('email'),
		        // isAdmin: localStorage.getItem('isAdmin')
		        email: match.email,
		        isAdmin: match.isAdmin
		    });

		    Router.push('/courses');
        } else {
		    console.log("Authentication failed, no match found.")
		}
		*/

		    //clear input fields after submission
	
		    // ternary operator = {condition ? ifTrue : ifFalse}
	return (
	    <Form onSubmit={e => authenticate(e)}>
	        <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
	        </Form.Group>

	        <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
		        <Form.Control 
                    type="password"
                    placeholder="Password" 
		            value={password}
		            onChange={(e) => setPassword(e.target.value)}
		            required
		        />
		    </Form.Group>

		    <Button className="bg-primary" type="submit">
		            Submit
		    </Button>
		</Form>
	)

}

